<?php

class User_model {
    private $nama = 'Gracia';
    private $db;
    private $table = 'user';

    public function __construct()
    {
        $this->db = new Database;
    }

    public function getUser()
    {
        return $this->nama;
    }

    public function cobaLogin($data)
    {
        $username = $data['Email'];
        $query = 'SELECT * FROM ' . $this->table . ' WHERE Email = :Email';
        $this->db->query($query);
        $this->db->bind(':Email', $username);
        $user = $this->db->single();
        $count = $this->db->rowCount();
        if($count == 0){
            return -1;
            
        }
        $passPost = $data['Password'];
        $passUser = $user['Password'];
        if(password_verify($passPost, $passUser)){
            $_SESSION['login'] = true;
            return 1;
        }else{
            return -1;
        }
    }

    public function register($data)
    {
        $query = 'INSERT INTO ' . $this->table . ' (Email, Username, Password) VALUES(:Email, :Username, :Password)';
        $haspassword = password_hash($data['Password'], PASSWORD_DEFAULT);
        $this->db->query($query);
        $this->db->bind('Email', $data['Email']);
        $this->db->bind('Username', $data['Username']);
        $this->db->bind('Password', $haspassword);
        $this->db->execute();
        return $this->db->rowCount();
    }

    public function logout()
    {
        session_destroy();
        header('location:' . BASEURL . '/login/index');
        exit;
    }
}