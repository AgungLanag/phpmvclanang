<section class="vh-100" style="background-color: #fff;">
    <div class="container py-5 h-100">
        <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col col-xl-10">
            <div class="card" style="border-radius: 1rem;">
            <div class="row g-0">
                <div class="col-md-6 col-lg-7 mx-auto">
                <div class="card-body p-4 p-lg-5 text-black">
                    <form method="post" action="regist/regist">
                    <h5 class="fw-normal mb-3 pb-3" style="letter-spacing: 1px;">Sign Up Your Account</h5>
                    <div class="form-outline mb-4">
                        <input type="email" id="form2Example17" name="Email" class="form-control form-control-lg"required />
                        <label class="form-label" for="form2Example17">Enter Email</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input type="username" id="form2Example17" name="Username" class="form-control form-control-lg" required/>
                        <label class="form-label" for="form2Example17">Enter Username</label>
                    </div>
                    <div class="form-outline mb-4">
                        <input type="password" id="form2Example27" name="Password" class="form-control form-control-lg" required/>
                        <label class="form-label" for="form2Example27">Enter Password</label>
                    </div>
                    <button class="btn btn-dark btn-lg" type="submit">Submit</button>
                    </form>
                </div>
                </div>
            </div>
            </div>
        </div>
    </div>
    </div>
</section>