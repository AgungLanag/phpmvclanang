<?php 
class Login extends Controller {

    public function __construct()
    {
        if (isset($_SESSION['login'])){
            header("location: " . BASEURL . "/home/index");
        }
    }

    public function index() {

        $data['judul'] = "Login";

        $this->view('templates/header',$data);
        $this->view('login/index', $data);
        $this->view('templates/footer');

    }

    public function prosesLogin()
    {
        if($this->model('User_model')->cobaLogin($_POST) > 0){
            header('Location:' . BASEURL . '/home/index');
        }else{
            header('Location:' . BASEURL . '/login/index');
        }
        

    }

    public function logout()
    {
        $this->model('User_model')->logout();
    }
}
