<?php 
class Regist extends Controller{
    public function index() {

        $data['judul'] = "Regist";

        $this->view('templates/header',$data);
        $this->view('regist/index', $data);
        $this->view('templates/footer');

    }

    public function regist()
    {
        $this->model('User_model')->register($_POST);
        header('Location:' . BASEURL . '/login/index');
    }

}

