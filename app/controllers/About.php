<?php

class About extends Controller {

    public function __construct()
    {
        if (!$_SESSION['login']){
            header("location: " . BASEURL . "/login/index");
        }
    }

    public function index($nama = 'AgungLanang', $pekerjaan = 'yang Belum Bekerja')
    {
        $data['nama'] = $nama;
        $data['pekerjaan'] = $pekerjaan;
        $data['judul'] = 'About'; 
        $this->view('templates/header', $data);
        $this->view('About/index', $data);
        $this->view('templates/footer');
    }

    public function page()
    {
        $data['judul'] = 'pages';
        $this->view('templates/header', $data);
        $this->view('About/page');
        $this->view('templates/footer');
    }
}

?>         